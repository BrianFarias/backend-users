package com.techprimers.mybatis.springbootmybatis.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class UsersDTO {

    private String name;
    private Integer salary;
   // @JsonIgnore
    private Integer id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
