package com.techprimers.mybatis.springbootmybatis.resource;

import com.techprimers.mybatis.springbootmybatis.mapper.UsersMapper;
import com.techprimers.mybatis.springbootmybatis.model.UsersDTO;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/rest/users")
public class UsersResource{

    private UsersMapper usersMapper;

    public UsersResource(UsersMapper usersMapper)
    {
        this.usersMapper = usersMapper;
    }


    @RequestMapping("/index")
    public List<UsersDTO> getAll() {
        return usersMapper.findAll();
    }

    @RequestMapping("/index/{id}")
    public UsersDTO getOne(@PathVariable String id) {

        return (UsersDTO) usersMapper.getOne(Integer.parseInt(id));
    }


    @RequestMapping(method = RequestMethod.POST,value = "/index")
    private List<UsersDTO> insert(@RequestBody UsersDTO users) {

        usersMapper.insert(users);

        return usersMapper.findAll();
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/index/{id}")
    private List<UsersDTO> updateUsers(@PathVariable Integer id, @RequestBody UsersDTO users) {
        users.setId(id);
        usersMapper.update(users);
        return usersMapper.findAll();

    }

    @RequestMapping(method = RequestMethod.DELETE, value ="/index/{id}")
    private List<UsersDTO> delete(@PathVariable Integer id) {
        usersMapper.delete(id);
        return usersMapper.findAll();

    }
}
